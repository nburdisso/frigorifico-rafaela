﻿using System.Web;
using System.Web.Mvc;

namespace Frigorifico_Rafaela
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
