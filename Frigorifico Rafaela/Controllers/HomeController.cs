﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;

namespace Frigorifico_Rafaela.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //string connectionstring = ConfigurationManager.ConnectionStrings["Oracle"].ConnectionString;

            //OracleConnection conn = new OracleConnection(connectionstring); // C#

            //conn.Open();

            //string sql = "select * from COhRTES";

            //OracleCommand cmd = new OracleCommand(sql,conn);

            //OracleDataReader dr = cmd.ExecuteReader();

            //dr.Read();


            //conn.Dispose();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}