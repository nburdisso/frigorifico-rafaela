﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Frigorifico_Rafaela.Startup))]
namespace Frigorifico_Rafaela
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
